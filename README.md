**Code that analyzes sentiment of Commit messages and relates with FICs**

Details:
1. fetches all Commits from default branch
2. labels sentiment value (-1,0,1) using the sentiment output from [GHSentiment](https://gitlab.com/FatiulHuqSujoy/ghsentiment)
    + removes Commits with no sentiment (not been computed)
3. analyzes Commits to extract parent Commits
4. labels type (FIC, fix, FIF)
5. outputs csv (2 columns: Special Commit sentiment and Regular Commit sentiment)
    + fic_all-emotions
    + fic_non_neutral
    + fif_all-emotions
    + fif_non_neutral
    + fix_all-emotions
    + fix_non_neutral
    + parent_all-emotions
    + parent_non_neutral

To-do:
1. analyze average sentiment of origins
    + computation heavy: analyze all diffs for all Commits