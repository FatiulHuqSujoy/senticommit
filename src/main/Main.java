package main;

import utils.ProjectLister;
import ghProcessors.TaskProcessor;
import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        main.execute();
    }

    private void execute() {
        for(String projectPath : ProjectLister.listProjects()){
            TaskProcessor taskProcessor = new TaskProcessor(projectPath);
            try {
                taskProcessor.execute();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (GitAPIException e) {
                e.printStackTrace();
            }
        }
    }
}
