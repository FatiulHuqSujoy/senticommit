package ghProcessors;

import ghObjects.GHCommit;
import outputProcessors.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public class OutputPrinter {
    private String projectName;

    public OutputPrinter(String projectName) {
        this.projectName = projectName;
    }

    public void printOutputs(List<GHCommit> commits) throws FileNotFoundException {
        for(OutputProcessor outputProcessor : getAllOutputProcessors()){
            writeSentimentListToFile(outputProcessor.getFolderName(), outputProcessor.getSentimentLists(commits));
        }
    }

    private void writeSentimentListToFile(String folderName, List<String>[] sentimentLists) throws FileNotFoundException {
        File file = getOrCreateFile(folderName);
        PrintWriter csvContentWriter = new PrintWriter(file);

        csvContentWriter.println("Special, Regular");

        int max = getMaxListSize(sentimentLists);
        for(int i=0; i<max; i++){
            String specSenti = getSentiFromList(sentimentLists[0], i);
            String regSenti = getSentiFromList(sentimentLists[1], i);

            csvContentWriter.println(specSenti + "," + regSenti);
        }

        csvContentWriter.close();
    }

    private File getOrCreateFile(String folderName) {
        String dirName = "results/" + folderName;
        File folder = new File(dirName);
        if(!folder.exists())
            folder.mkdirs();

        File file = new File(dirName + "/" + projectName + ".csv");
        return file;
    }

    private String getSentiFromList(List<String> sentiList, int index) {
        if(index >= sentiList.size())
            return "";
        return sentiList.get(index);
    }

    private int getMaxListSize(List<String>[] sentimentLists) {
        if(sentimentLists[0].size()>sentimentLists[1].size())
            return sentimentLists[0].size();
        return sentimentLists[1].size();
    }

    private OutputProcessor[] getAllOutputProcessors() {
        return new OutputProcessor[]{
//                new FicAllOutput(),
//                new FicEmotionOutput(),
//                new FixAllOutput(),
//                new FixEmotionOutput(),
                new FifAllOutput(),
                new FifEmotionOutput(),
//                new ParentAllOutput(),
//                new ParentEmotionOutput(),
                new FifVsFixAllOutput(),
                new FifVsFixEmotionOutput()
        };
    }
}
