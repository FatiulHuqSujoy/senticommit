package ghProcessors;

import ghObjects.GHCommit;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ListBranchCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CommitFetcher {
    private Git git;
    private Repository repository;

    public CommitFetcher(Git git) {
        this.git = git;
        repository = git.getRepository();
    }

    public List<GHCommit> fetchCommits() throws IOException, GitAPIException {
        List<GHCommit> commits = new ArrayList<>();

//        Iterable<RevCommit> revCommits = git.log().all().call();
        Iterable<RevCommit> revCommits = getRevCommits();

        if(revCommits!=null){
            for(RevCommit revCommit : revCommits){
                GHCommit commit = new GHCommit(revCommit);
                commits.add(commit);
            }
        }

        return commits;
    }

    private Iterable<RevCommit> getRevCommits() throws GitAPIException, IOException {
        Iterable<RevCommit> revCommits = null;

        try{
            ObjectId branchId = getDefaultBranchId();
            revCommits = git
                    .log()
                    .add(branchId)
                    .call();
        }
        catch (NullPointerException ne){
            System.err.println("no master");
        }

        return revCommits;
    }

    private ObjectId getDefaultBranchId() throws GitAPIException {
        List<Ref> branches = git.branchList().setListMode(ListBranchCommand.ListMode.ALL).call();

        return branches.get(0).getObjectId();

    }

    public void printBranches() throws GitAPIException {
        int c = 0;
        List<Ref> call = git.branchList().call();
        for (Ref ref : call) {
            System.out.println("\tBranch: " + ref + " " + ref.getName() + " "
                    + ref.getObjectId().getName());
            c++;
        }
        System.out.println("Number of branches: " + c);
    }
}
