package ghProcessors;

import ghObjects.GHCommit;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SentimentLabeler {
    private String projectName;
    private Map<String, String> sentiMap;

    public SentimentLabeler(String projectName) {
        this.projectName = projectName;
        sentiMap = new HashMap<>();
    }

    public void populateSentiMap() throws IOException {
        File file = new File("senti_maps/" + projectName + ".csv");

        String line = "";
        BufferedReader br = new BufferedReader(new FileReader(file));
        while ((line = br.readLine()) != null) {
            String[] contents = line.split(",");
            sentiMap.put(contents[0], contents[1]);
        }
    }

    public void labelCommitSentiment(List<GHCommit> commits){
        cleanseList(commits);
        for(GHCommit commit : commits){
            int sentiment = extractSentiment(commit.getSha());
            commit.setSentiment(sentiment);
        }
    }

    private void cleanseList(List<GHCommit> commits) {
        for (Iterator<GHCommit> iterator = commits.iterator(); iterator.hasNext();) {
            GHCommit commit = iterator.next();
            if(!sentiMap.containsKey(commit.getSha()))
                iterator.remove();
        }
    }

    private int extractSentiment(String sha) {
        String sentiment = sentiMap.get(sha);

//        if(sentiment==null)
//            return 100;

        if(sentiment.equals("positive"))
            return 1;
        if(sentiment.equals("negative"))
            return -1;
        return 0;
    }
}
