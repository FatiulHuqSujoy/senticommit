package ghProcessors;

import ghObjects.GHCommit;
import org.eclipse.jgit.api.BlameCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.blame.BlameResult;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.Edit;
import org.eclipse.jgit.diff.RawTextComparator;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommitAnalyzer {
    private String projectPath;
    private Git git;
    private Repository repository;
    private Map<String, GHCommit> shaMap;

    public CommitAnalyzer(String projectPath, Git git, Repository repository) {
        this.projectPath = projectPath;
        this.git = git;
        this.repository = repository;
        shaMap = new HashMap<>();
    }

    public void populateShaMap(List<GHCommit> commits){
        for(GHCommit commit : commits){
            shaMap.put(commit.getSha(), commit);
        }
    }

    public void analyzeCommits(List<GHCommit> commits) throws IOException, GitAPIException {
        for(GHCommit commit : commits){
            setParentage(commit);
//            setOrigins(commit);
        }
    }

    private void setParentage(GHCommit commit) {
        if(commit.getRevCommit().getParentCount()>0){
            String parentSha = commit.getRevCommit().getParent(0).getName();
            commit.setParent(shaMap.get(parentSha));
        }
    }

    private void setOrigins(GHCommit commit) throws IOException, GitAPIException {
        ObjectId parentCommit = commit.getParent().getRevCommit().getTree();
        ObjectId childCommit = commit.getRevCommit().getTree();
        listDiffs(parentCommit, childCommit, commit);
    }

    private void listDiffs(ObjectId parentCommit, ObjectId currentCommit, GHCommit commit) throws IOException, GitAPIException {
        DiffFormatter df = new DiffFormatter(System.out);
        df.setRepository(repository);
        df.setDiffComparator(RawTextComparator.DEFAULT);
        df.setDetectRenames(true);
        List<DiffEntry> diffs = df.scan(parentCommit, currentCommit);

        for (DiffEntry diff : diffs) {
            String filePath = diff.getNewPath();
            if(!filePath.endsWith(".java"))
                continue;

            for (Edit edit : df.toFileHeader(diff).toEditList()) {
                if(edit.getType().equals(Edit.Type.INSERT))
                    continue;

                int start = edit.getBeginA();
                int finish = edit.getEndA();
                blameChange(start, finish, filePath, commit);
            }
        }
    }

    private void blameChange(int start, int finish, String filePath, GHCommit commit) throws GitAPIException, IOException {
        BlameCommand blamer = new BlameCommand(repository);
        blamer.setStartCommit(repository.resolve(commit.getSha()));
        blamer.setFilePath(filePath);
        BlameResult blame = blamer.call();

        for (int i=start+1; i<=finish; i++) {
            RevCommit blameCommit = blame.getSourceCommit(i);
            GHCommit origin = shaMap.get(blameCommit.getName());
            if(origin!=null)
                commit.addOrigin(origin);
        }
    }
}
