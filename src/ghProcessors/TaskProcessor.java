package ghProcessors;

import ghObjects.GHCommit;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class TaskProcessor {
    private Git git;
    private Repository repository;

    private List<GHCommit> ghCommits, fics, fixes, fifs;
    private String projectPath, projectName;

    public TaskProcessor(String projectPath) {
        this.projectPath = projectPath;
        projectName = projectPath.split("/")[1];
    }

    public void execute() throws IOException, GitAPIException {
        System.out.println("\nANALYZING " + projectPath);

        openRepo();

        fetchAllCommits();

        labelCommitSentiment();

        analyzeCommits();

        labelCommitType();

        printOutput();
    }

    private void openRepo() throws IOException, GitAPIException {
        RepoOpener repoOpener = new RepoOpener();
        repoOpener.openRepo(projectPath);
        git = repoOpener.getGit();
        repository = repoOpener.getRepository();
    }

    private void fetchAllCommits() throws IOException, GitAPIException {
        System.out.println("\tFetching Commits...");
        CommitFetcher commitFetcher = new CommitFetcher(git);
//        commitFetcher.printBranches();
        ghCommits = commitFetcher.fetchCommits();
    }

    private void labelCommitSentiment() throws IOException {
        System.out.println("\tFetching Sentiment...");
        SentimentLabeler sentimentLabeler = new SentimentLabeler(projectName);
        sentimentLabeler.populateSentiMap();
        sentimentLabeler.labelCommitSentiment(ghCommits);
    }

    private void analyzeCommits() throws IOException, GitAPIException {
        System.out.println("\tAnalyzing Commits...");
        CommitAnalyzer commitAnalyzer = new CommitAnalyzer(projectPath, git, repository);
        commitAnalyzer.populateShaMap(ghCommits);
        commitAnalyzer.analyzeCommits(ghCommits);
    }

    private void labelCommitType() throws FileNotFoundException {
        System.out.println("\tLabeling Commits...");
        CommitLabeler commitLabeler = new CommitLabeler(projectName);
        commitLabeler.populateShas();
        commitLabeler.labelCommits(ghCommits);
    }

    private void printOutput() throws FileNotFoundException {
        System.out.println("\tPrinting Output...");
        OutputPrinter outputPrinter = new OutputPrinter(projectName);
        outputPrinter.printOutputs(ghCommits);
    }
}
