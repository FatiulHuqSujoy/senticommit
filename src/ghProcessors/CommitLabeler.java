package ghProcessors;

import ghObjects.GHCommit;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CommitLabeler {
    private String projectName;
    private List<String> ficShas, fixShas;

    public CommitLabeler(String projectName) {
        this.projectName = projectName;
        ficShas = new ArrayList<>();
        fixShas = new ArrayList<>();
    }

    public void populateShas() throws FileNotFoundException {
        fetchShas(ficShas, "/fic_shas.txt");
        fetchShas(fixShas, "/fix_shas.txt");
    }

    private void fetchShas(List<String> shas, String fileName) throws FileNotFoundException {
        File file = new File("fic_resources/" + projectName + fileName);
        Scanner s = new Scanner(file);
        while (s.hasNext())
            shas.add(s.next());
        s.close();
    }

    public void labelCommits(List<GHCommit> commits){
        for(GHCommit commit : commits){
            commit.setFic(ficShas.contains(commit.getSha()));
            commit.setFix(fixShas.contains(commit.getSha()));
            commit.setFif();
        }
    }
}
