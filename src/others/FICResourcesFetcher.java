package others;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FICResourcesFetcher {
    List<String> ficShas, fixShas;
    public static void main(String[] args) {
        FICResourcesFetcher ficResourcesFetcher = new FICResourcesFetcher();
        try {
            ficResourcesFetcher.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void execute() throws IOException {
        for(String projectName : getProjectNames()){
            init();
            copyFicResources(projectName.split("/")[1]);
        }
    }

    private void init() {
        ficShas = new ArrayList<>();
        fixShas = new ArrayList<>();
    }

    private void copyFicResources(String projectName) throws IOException {
        createResourceDirectory(projectName);

        getExistingShas(projectName, "BIC", ficShas);
        pasteAsNewResource(projectName, "fic_shas.txt", ficShas);

        getExistingShas(projectName, "fixCommits", fixShas);
        pasteAsNewResource(projectName, "fix_shas.txt", fixShas);
    }

    private void createResourceDirectory(String projectName) {
        File folder = new File("fic_resources/" + projectName);
        if(!folder.exists())
            folder.mkdirs();
    }

    private void pasteAsNewResource(String projectName, String fileName, List<String> list) throws FileNotFoundException {
        File resourceFile = new File("fic_resources/" + projectName + "/" + fileName);
        PrintWriter resourceWriter = new PrintWriter(resourceFile);

        for(String sha : list){
            resourceWriter.println(sha);
        }

        resourceWriter.close();
    }

    private void getExistingShas(String projectName, String type, List<String> list) throws IOException {
        File ficShaFile = new File("../FICResources/" + projectName + "_details/" + type + ".txt");

        String line = "";

        BufferedReader br = new BufferedReader(new FileReader(ficShaFile));
        while ((line = br.readLine()) != null) {
            for(String sha : line.split(" ")){
                list.add(sha);
            }
        }
    }

    private String[] getProjectNames() {
        return new String[]{
                "apache/tomcat",
                "google/guava",
                "mockito/mockito",
                "apache/commons-lang",
                "apache/hadoop",
//                "elastic/elasticsearch",
                "spring-projects/spring-framework",
//                "SeleniumHQ/selenium",
//                "github/android",
                "netty/netty",
                "Bukkit/Bukkit",
                "clojure/clojure",
                "facebook/facebook-android-sdk",
                "JakeWharton/ActionBarSherlock",
//                "nathanmarz/storm"
        };
    }
}
