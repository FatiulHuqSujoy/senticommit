package others;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class SentiMapFetcher {
    public static void main(String[] args) {
        SentiMapFetcher sentiMapFetcher = new SentiMapFetcher();
        try {
            sentiMapFetcher.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void execute() throws IOException {
        for(String projectName : getProjectNames()){
            copySentiMap(projectName.split("/")[1]);
        }
    }

    private void copySentiMap(String projectName) throws IOException {
        List<String> lines = getExistingSentiMap(projectName);
        pasteToNewSentiMap(projectName, lines);
    }

    private void pasteToNewSentiMap(String projectName, List<String> lines) throws FileNotFoundException {
        File sentiMapFile = new File("senti_maps/" + projectName + ".csv");
        PrintWriter csvSentiMapWriter = new PrintWriter(sentiMapFile);

        for(String line : lines){
            csvSentiMapWriter.println(line);
        }

        csvSentiMapWriter.close();
    }

    private List<String> getExistingSentiMap(String projectName) throws IOException {
        File sentiMapFile = new File("../CommitAnalysisResult/" + projectName + "/sentiMap.csv");

        List<String> lines = new ArrayList<>();
        String line = "";

        BufferedReader br = new BufferedReader(new FileReader(sentiMapFile));
        while ((line = br.readLine()) != null) {
            lines.add(line);
        }

        return lines;
    }

    private String[] getProjectNames() {
        return new String[]{
                "apache/tomcat",
                "google/guava",
                "mockito/mockito",
                "apache/commons-lang",
                "apache/hadoop",
                "elastic/elasticsearch",
                "spring-projects/spring-framework",
                "SeleniumHQ/selenium",
                "github/android",
                "netty/netty",
                "Bukkit/Bukkit",
                "clojure/clojure",
                "facebook/facebook-android-sdk",
                "JakeWharton/ActionBarSherlock",
                "nathanmarz/storm"
        };
    }
}
