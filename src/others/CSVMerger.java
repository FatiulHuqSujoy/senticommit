package others;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CSVMerger {
    public static void main(String[] args) {
        CSVMerger csvMerger = new CSVMerger();
        try {
            csvMerger.mergeCSVs();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void mergeCSVs() throws IOException {
        File rootDir = new File("results");
        for(File folder : rootDir.listFiles()){
            if(folder.isDirectory() && !folder.getName().equals("merged")){
                List<String>[] contents = getContents(folder);
                populateContent(contents, folder.getName(), rootDir.getPath());
            }
        }
    }

    private List<String>[] getContents(File folder) throws IOException {
        List<String> col1 = new ArrayList<>(), col2 = new ArrayList<>();

        for(File file : folder.listFiles()){
            String line = "";
            BufferedReader br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null) {
                if(line.equals("Special, Regular"))
                    continue;
                String element1 = line.split(",")[0];
                String element2 = line.split(",")[1];
                if(!element1.equals(""))
                    col1.add(element1);
                if(!element2.equals(""))
                    col2.add(element2);
            }
            br.close();
        }
        List[] contents = {
                col1,
                col2
        };
        return contents;
    }

    private void populateContent(List<String>[] contents, String fileName, String rootPath) throws FileNotFoundException {
        File file = new File(rootPath + "/merged/" + fileName + ".csv");
        PrintWriter csvContentWriter = new PrintWriter(file);

        csvContentWriter.println("Special, Regular");

        int max = getMaxListSize(contents);
        for(int i=0; i<max; i++){
            String specSenti = getSentiFromList(contents[0], i);
            String regSenti = getSentiFromList(contents[1], i);

            csvContentWriter.println(specSenti + "," + regSenti);
        }

        csvContentWriter.close();
    }

    private String getSentiFromList(List<String> sentiList, int index) {
        if(index >= sentiList.size())
            return "";
        return sentiList.get(index);
    }

    private int getMaxListSize(List<String>[] sentimentLists) {
        if(sentimentLists[0].size()>sentimentLists[1].size())
            return sentimentLists[0].size();
        return sentimentLists[1].size();
    }
}
