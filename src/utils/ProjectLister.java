package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProjectLister {

    public static List<String> listProjects() {
        Scanner s = null;
        try {
            s = new Scanner(new File("projects.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        List<String> projectNames = new ArrayList<>();
        while (s.hasNext()){
            projectNames.add(s.next());
        }
        s.close();

        return projectNames;
    }

}
