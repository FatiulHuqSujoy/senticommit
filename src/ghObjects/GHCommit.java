package ghObjects;

import org.eclipse.jgit.revwalk.RevCommit;

import java.util.ArrayList;
import java.util.List;

public class GHCommit {
    private String sha, msg;
    private RevCommit revCommit;
    private boolean isFic, isFix, isFif;

    private GHCommit parent;
    private List<GHCommit> origins;

    private int sentiment;

    public GHCommit(RevCommit revCommit) {
        this.revCommit = revCommit;
        sha = revCommit.getName();
        msg = revCommit.getFullMessage();

        origins = new ArrayList<>();
    }

    public boolean equals(GHCommit commit){
        return sha.equals(commit.getSha());
    }

    public String getSha() {
        return sha;
    }

    public String getMsg() {
        return msg;
    }

    public RevCommit getRevCommit() {
        return revCommit;
    }

    public boolean isFic() {
        return isFic;
    }

    public boolean isFix() {
        return isFix;
    }

    public boolean isFif() {
        return isFif;
    }

    public GHCommit getParent() {
        return parent;
    }

    public List<GHCommit> getOrigins() {
        return origins;
    }

    public int getSentiment() {
        return sentiment;
    }

    public void setFic(boolean fic) {
        isFic = fic;
    }

    public void setFix(boolean fix) {
        isFix = fix;
    }

    public void setFif() {
        isFif = isFic && isFix;
    }

    public void setSentiment(int sentiment) {
        this.sentiment = sentiment;
    }

    public void setParent(GHCommit parent) {
        this.parent = parent;
    }

    public void addOrigin(GHCommit origin){
        origins.add(origin);
    }
}
