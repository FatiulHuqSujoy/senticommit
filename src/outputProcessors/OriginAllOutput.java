package outputProcessors;

import ghObjects.GHCommit;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class OriginAllOutput implements OutputProcessor {
    @Override
    public String getFolderName() {
        return "origin_all_emotion";
    }

    @Override
    public List<String>[] getSentimentLists(List<GHCommit> commits) {
        List<String> special = new ArrayList<>(), regular = new ArrayList<>();

        for(GHCommit commit : commits){
            String sentiment = getOriginSentimentAverage(commit.getOrigins());
            if(commit.isFic())
                special.add(sentiment);
            else
                regular.add(sentiment);
        }

        return new List[]{special, regular};
    }

    private String getOriginSentimentAverage(List<GHCommit> origins) {
        int total = 0, count = 0;

        for(GHCommit origin : origins){
            total += origin.getSentiment();
            count++;
        }

        DecimalFormat numberFormat = new DecimalFormat("#.00");
        return numberFormat.format((double)total/count);
    }
}
