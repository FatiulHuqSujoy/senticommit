package outputProcessors;

import ghObjects.GHCommit;

import java.util.ArrayList;
import java.util.List;

public class FixEmotionOutput implements OutputProcessor {
    @Override
    public String getFolderName() {
        return "fix_non_neutral";
    }

    @Override
    public List<String>[] getSentimentLists(List<GHCommit> commits) {
        List<String> special = new ArrayList<>(), regular = new ArrayList<>();

        for(GHCommit commit : commits){
            if(commit.getSentiment()==0)
                continue;

            String sentiment = commit.getSentiment() + "";
            if(commit.isFix())
                special.add(sentiment);
            else
                regular.add(sentiment);
        }

        return new List[]{special, regular};
    }
}
