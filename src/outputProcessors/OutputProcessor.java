package outputProcessors;

import ghObjects.GHCommit;

import java.util.List;

public interface OutputProcessor {
    public abstract String getFolderName();
    public abstract List<String>[] getSentimentLists(List<GHCommit> commits);
}
