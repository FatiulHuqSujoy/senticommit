package outputProcessors;

import ghObjects.GHCommit;

import java.util.ArrayList;
import java.util.List;

public class FixAllOutput implements OutputProcessor {
    @Override
    public String getFolderName() {
        return "fix_all_emotion";
    }

    @Override
    public List<String>[] getSentimentLists(List<GHCommit> commits) {
        List<String> special = new ArrayList<>(), regular = new ArrayList<>();

        for(GHCommit commit : commits){
            String sentiment = commit.getSentiment() + "";
            if(commit.isFix())
                special.add(sentiment);
            else
                regular.add(sentiment);
        }

        return new List[]{special, regular};
    }
}
