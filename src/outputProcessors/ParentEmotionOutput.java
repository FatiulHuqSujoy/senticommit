package outputProcessors;

import ghObjects.GHCommit;

import java.util.ArrayList;
import java.util.List;

public class ParentEmotionOutput implements OutputProcessor {
    @Override
    public String getFolderName() {
        return "parent_non_neutral";
    }

    @Override
    public List<String>[] getSentimentLists(List<GHCommit> commits) {
        List<String> special = new ArrayList<>(), regular = new ArrayList<>();

        for(GHCommit commit : commits){
            if(commit.getParent()==null || commit.getSentiment()==0)
                continue;

            String sentiment = commit.getParent().getSentiment() + "";
            if(commit.isFic())
                special.add(sentiment);
            else
                regular.add(sentiment);
        }

        return new List[]{special, regular};
    }
}
